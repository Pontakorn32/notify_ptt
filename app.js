const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');

const session = require('express-session');
const flash = require('connect-flash');
const mysql = require('mysql');
const connection = require('express-myconnection')
const app = express();
const upload = require("express-fileupload");

app.use(upload());
app.use(cookie());
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(session({
  secret: 'Passw0rd',
  cookie: {maxAge : 72 * 60 * 60 * 1000},
  resave: true,
  saveUninitialized: true
}));
app.use(connection(mysql, {
  host: 'localhost',
  user: 'root',
  password: '1234',
  port: 3306,
  database: 'notify_ptt'
}, 'single'));
app.use(flash());

const PORT = 8081;



const loginRoute = require('./routes/loginRoutes');
app.use('/', loginRoute);
const registerRoute = require('./routes/registerRoutes');
app.use('/', registerRoute);
const alertsRoute = require('./routes/alertsRoutes');
app.use('/', alertsRoute);
const grouplineRoute = require('./routes/grouplineRoutes');
app.use('/', grouplineRoute);
const branchRoute = require('./routes/branchRoutes');
app.use('/', branchRoute);
const personnelRoute = require('./routes/personnelRoutes');
app.use('/', personnelRoute);
const notification = require('./routes/notificationRoutes');
app.use('/', notification);
const history = require('./routes/historyRoutes');
app.use('/', history);
const bag = require('./routes/bagRoutes');
app.use('/', bag);

app.listen(PORT, () => console.log(`Server Listen PORT ${PORT}`));
