const express = require('express');
const router = express.Router();

const groupalertsViews = require('../controllers/grouplineControllers');

router.get('/groupline',groupalertsViews.grouplinelist); //!เข้าหน้ฟอร์ม groupalerts
router.post('/groupline/save', groupalertsViews.groupalertssave); //!เพิ่ม groupalerts
router.get('/groupline/edit:id',groupalertsViews.edit); //!เข้าหน้าแก้ไขฟอร์ม groupalerts
router.post('/groupline/update:id',groupalertsViews.update); //!แก้ไขฟอร์ม groupalerts
router.get('/groupline/delete:id',groupalertsViews.delete); //!ลบ groupalerts

module.exports = router;