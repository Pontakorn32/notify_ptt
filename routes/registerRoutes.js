const express = require('express');
const router = express.Router();

const registerViews = require('../controllers/registerControllers');

router.get('/register',registerViews.register); //เข้าหน้าลงทะเบียน
router.post('/register/save', registerViews.registersave); 

module.exports = router;