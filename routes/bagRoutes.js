const express = require('express');
const router = express.Router();

const bagContro = require('../controllers/bagControllers');

router.get('/bag',bagContro.bag_list); //!เข้าหน้ฟอร์ม bag
router.get('/bag/detail:id',bagContro.bag_detail); //!เข้าหน้าฟอร์มรายละเอียด bag
router.post('/bag/success:id',bagContro.bag_success); //!งานสำเร็จ
router.post('/bag/fail:id',bagContro.bag_fail); //!งานไม่สำเร็จ

module.exports = router;