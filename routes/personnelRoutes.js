const express = require('express');
const router = express.Router();

const personnelContro = require('../controllers/personnelControllers');

router.get('/personnel',personnelContro.personnel_list); //!เข้าหน้ฟอร์ม personnel
router.post('/personnel/save', personnelContro.personnel_save); //!เพิ่ม personnel
router.get('/personnel/edit:id',personnelContro.personnel_edit); //!เข้าหน้ฟอร์มแก้ไข personnel
router.post('/personnel/update:id', personnelContro.personnel_update); //!แก้ไข personnel
router.get('/personnel/delete:id',personnelContro.personnel_delete); //!ลบ personnel


module.exports = router;