const express = require('express');
const router = express.Router();

const notificationContro = require('../controllers/notificationControllers');

router.get('/notification',notificationContro.notification_list); //!เข้าหน้ฟอร์ม notification
router.get('/notification/detail:id',notificationContro.notification_detail); //!เข้าหน้าฟอร์มรายละเอียด notification
router.get('/notification/delete:id',notificationContro.notification_delete); //!ลบ notification
router.get('/notification/select:alid/:psid',notificationContro.notification_select); //!เลือกผู้รับผิดชอบงาน
router.post('/notification/success:id',notificationContro.notification_success); //!งานสำเร็จ
router.post('/notification/fail:id',notificationContro.notification_fail); //!งานไม่สำเร็จ

module.exports = router;