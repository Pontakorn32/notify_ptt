const express = require('express');
const router = express.Router();

const branchControl = require('../controllers/branchControllers');

router.get('/branch',branchControl.branchlist); //!เข้าหน้ฟอร์ม branch
router.post('/branch/save', branchControl.branchsave); //!เพิ่ม branch
router.get('/branch/edit:id',branchControl.branchedit); //!เข้าหน้าแก้ไขฟอร์ม branch
router.post('/branch/update:id',branchControl.branchupdate); //!แก้ไขฟอร์ม branch
router.get('/branch/delete:id',branchControl.branchdelete); //!ลบ branch

//! ---------------------------------------------------------------------------แผนก----------------------------------------------------------------
router.get('/branch/department:id',branchControl.departmentlist); //!เข้าหน้ฟอร์ม department
router.post('/branch/department/save', branchControl.departmentsave); //!เพิ่ม department
router.get('/branch/department/edit:id',branchControl.departmentedit); //!เข้าหน้าแก้ไขฟอร์ม department
router.post('/branch/department/update:id',branchControl.departmentupdate); //!แก้ไขฟอร์ม department
router.get('/branch/department/delete:id',branchControl.departmentdelete); //!ลบ department

module.exports = router;