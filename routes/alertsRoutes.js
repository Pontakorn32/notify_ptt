const express = require('express');
const router = express.Router();

const alertsViews = require('../controllers/alertsControllers');

router.get('/alerts',alertsViews.alerts); //!เข้าหน้ฟอร์ม alerts
router.post('/alerts/save', alertsViews.alertssave); //!รายงานปัญหา

router.get('/api/department/:idbranch',alertsViews.department);

module.exports = router;