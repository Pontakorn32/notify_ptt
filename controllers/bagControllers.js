const controller = {};
const request = require('request')
const uuidv4 = require('uuid').v4;
var nodemailer = require('nodemailer');

controller.bag_list = (req, res) => {
    if (req.cookies.idpersonnel_ptt && (req.cookies.permission == 3 || req.cookies.permission == 2)) {
        const uid = req.cookies.idpersonnel_ptt;
        req.getConnection((err, conn) => {
            conn.query('SELECT idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, ps.name_ps, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment WHERE statusalertsid_al = 2 AND personnelid_al = ? ORDER BY idalerts DESC', [uid], (err2, alerts2) => {
                conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, name_ps, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment  WHERE statusalertsid_al = 3 AND personnelid_al = ? ORDER BY idalerts DESC', [uid], (err3, alerts3) => {
                    conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, name_ps, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment  WHERE statusalertsid_al = 4 AND personnelid_al = ? ORDER BY idalerts DESC', [uid], (err4, alerts4) => {
                        if (err2) console.log(err2)
                        else res.render('bag/bag_list', { cookie: req.cookies, alerts2: alerts2, alerts3: alerts3, alerts4: alerts4, success: req.flash('success'), fail: req.flash('fail') })
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.bag_detail = (req, res) => {
    if (req.cookies.idpersonnel_ptt && (req.cookies.permission == 3 || req.cookies.permission == 2)) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), " %d/%m/%Y" )date, DATE_FORMAT(datetime_al, " %H:%i:%s น." )time, detail_al, ps.name_ps, ps.phone_ps, name_br, name_dpm, name_sta, name_group, comment_al ,ps2.name_ps name_ps2, ps2.phone_ps phone_ps2, DATE_FORMAT(DATE_ADD(timesuccess_al, INTERVAL 543 YEAR), "%d/%m/%Y" )date_success, DATE_FORMAT(timesuccess_al, "%H:%i:%s น." )time_success FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN statusalerts stal ON stal.idstatusalerts = al.statusalertsid_al JOIN groupalerts gr ON gr.idgroupalerts = al.groupalertsid_al LEFT JOIN personnel ps2 ON ps2.idpersonnel = al.personnelid_al WHERE idalerts = ?', [id], (err1, detail_alerts) => {
                conn.query('SELECT * FROM alerts_img WHERE alertsid_img = ?', [id], (err2, img_alerts) => {
                    conn.query('SELECT * FROM alerts_img_closework WHERE alertsid_img_cw = ?', [id], (err2, img_alerts_closework) => {
                        if (err1) console.log(err1)
                        else res.render('bag/bag_detail', { cookie: req.cookies, detail_alerts: detail_alerts[0], img_alerts: img_alerts, img_alerts_closework: img_alerts_closework })
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.bag_success = (req, res) => {
    if (req.cookies.idpersonnel_ptt && (req.cookies.permission == 3 || req.cookies.permission == 2)) {
        const { id } = req.params;
        const data = req.body;
        if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
                var filename = uuidv4() + "." + file.name.split(".")[1];
                file.mv("./public/img_alerts_closework/" + filename, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        req.getConnection((err, conn) => {
                            conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 3, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                                conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err6, result2) => {
                                        if (err6) {
                                            console.log(err6);
                                        } else {
                                            request({
                                                method: 'POST',
                                                url: 'https://notify-api.line.me/api/notify',
                                                header: {
                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                },
                                                auth: {
                                                    bearer: alerts[0].line_token, //token
                                                },
                                                form: {
                                                    message: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al, //ข้อความที่จะส่ง
                                                },
                                            }, (err, httpResponse, body) => {
                                                var transporter = nodemailer.createTransport({
                                                    host: "smtp.gmail.com",
                                                    port: 465,
                                                    secure: true,
                                                    auth: {
                                                        user: 'pontakorn.company42@gmail.com',
                                                        pass: 'pontakorn42'
                                                    }
                                                });

                                                var mailOptions = {
                                                    from: 'pontakorn.company42@gmail.com',
                                                    to: 'pontakorn322@gmail.com',
                                                    subject: 'จัดการงานสำเร็จ!',
                                                    text: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al //ข้อความที่จะส่ง
                                                };

                                                transporter.sendMail(mailOptions, function (error, info) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        // console.log('Email sent: ' + info.response);
                                                    }
                                                });
                                            })
                                            req.flash('success', 'ปิดงานสำเร็จ!!')
                                            setTimeout(() => {
                                                res.redirect('/bag',);
                                            }, 1000)
                                        }
                                    });
                                });
                            });
                        });
                    }
                });
            } else {
                //!ใส่ข้อมูลแบบหลายรูป
                req.getConnection((err, conn) => {
                    conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 3, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                        conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                            if (err) {
                                console.log(err);
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var filename = uuidv4() + "." + file[i].name.split(".")[1];
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err, result2) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                    })
                                    file[i].mv("./public/img_alerts_closework/" + filename, async function (err) {
                                        if (err) {
                                            console.log(err);
                                        }

                                    })
                                }
                                request({
                                    method: 'POST',
                                    url: 'https://notify-api.line.me/api/notify',
                                    header: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                    },
                                    auth: {
                                        bearer: alerts[0].line_token, //token
                                    },
                                    form: {
                                        message: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al, //ข้อความที่จะส่ง
                                    },
                                }, (err, httpResponse, body) => {
                                    var transporter = nodemailer.createTransport({
                                        host: "smtp.gmail.com",
                                        port: 465,
                                        secure: true,
                                        auth: {
                                            user: 'pontakorn.company42@gmail.com',
                                            pass: 'pontakorn42'
                                        }
                                    });

                                    var mailOptions = {
                                        from: 'pontakorn.company42@gmail.com',
                                        to: 'pontakorn322@gmail.com',
                                        subject: 'จัดการงานสำเร็จ!',
                                        text: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al //ข้อความที่จะส่ง
                                    };

                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            // console.log('Email sent: ' + info.response);
                                        }
                                    });
                                })
                                req.flash('success', 'ปิดงานสำเร็จ!!')
                                setTimeout(() => {
                                    res.redirect('/bag',);
                                }, 1000)

                            }
                        });
                    });
                });
            }
        }
    } else {
        res.redirect('/');
    }
};

controller.bag_fail = (req, res) => {
    if (req.cookies.idpersonnel_ptt && (req.cookies.permission == 3 || req.cookies.permission == 2)) {
        const { id } = req.params;
        const data = req.body;
        if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
                var filename = uuidv4() + "." + file.name.split(".")[1];
                file.mv("./public/img_alerts_closework/" + filename, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        req.getConnection((err, conn) => {
                            conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 4, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                                conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err6, result2) => {
                                        if (err1) {
                                            console.log(err1);
                                        } else {
                                            request({
                                                method: 'POST',
                                                url: 'https://notify-api.line.me/api/notify',
                                                header: {
                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                },
                                                auth: {
                                                    bearer: alerts[0].line_token, //token
                                                },
                                                form: {
                                                    message: "ไม่สามารถจัดการงานให้สำเร็จได้!! " + alerts[0].name_ps + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + alerts[0].comment_al, //ข้อความที่จะส่ง
                                                },
                                            }, (err, httpResponse, body) => {
                                                var transporter = nodemailer.createTransport({
                                                    host: "smtp.gmail.com",
                                                    port: 465,
                                                    secure: true,
                                                    auth: {
                                                        user: 'pontakorn.company42@gmail.com',
                                                        pass: 'pontakorn42'
                                                    }
                                                });

                                                var mailOptions = {
                                                    from: 'pontakorn.company42@gmail.com',
                                                    to: 'pontakorn322@gmail.com',
                                                    subject: 'จัดการงานไม่สำเร็จ!',
                                                    text: "ไม่สามารถจัดการงานให้สำเร็จได้!! " + alerts[0].name_ps + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + alerts[0].comment_al //ข้อความที่จะส่ง
                                                };

                                                transporter.sendMail(mailOptions, function (error, info) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        // console.log('Email sent: ' + info.response);
                                                    }
                                                });
                                            })
                                            req.flash('fail', 'ปิดงานไม่สำเร็จ!!')
                                            setTimeout(() => {
                                                res.redirect('/bag',);
                                            }, 1000)
                                        }
                                    });
                                });
                            });
                        });
                    }
                });
            } else {
                //!อัพโหลดหลายรูป
                req.getConnection((err, conn) => {
                conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 4, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                    conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                        if (err) {
                            console.log(err);
                        } else {
                            for (var i = 0; i < file.length; i++) {
                                var filename = uuidv4() + "." + file[i].name.split(".")[1];
                                conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err, result2) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                                file[i].mv("./public/img_alerts_closework/" + filename, async function (err) {
                                    if (err) {
                                        console.log(err);
                                    }
                                })
                            }
                        }
                        request({
                            method: 'POST',
                            url: 'https://notify-api.line.me/api/notify',
                            header: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            auth: {
                                bearer: alerts[0].line_token, //token
                            },
                            form: {
                                message: "ไม่สามารถจัดการงานให้สำเร็จได้!! " + alerts[0].name_ps + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + alerts[0].comment_al, //ข้อความที่จะส่ง
                            },
                        }, (err, httpResponse, body) => {
                            var transporter = nodemailer.createTransport({
                                host: "smtp.gmail.com",
                                port: 465,
                                secure: true,
                                auth: {
                                    user: 'pontakorn.company42@gmail.com',
                                    pass: 'pontakorn42'
                                }
                            });

                            var mailOptions = {
                                from: 'pontakorn.company42@gmail.com',
                                to: 'pontakorn322@gmail.com',
                                subject: 'จัดการงานไม่สำเร็จ!',
                                text: "ไม่สามารถจัดการงานให้สำเร็จได้!! " + alerts[0].name_ps + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + alerts[0].comment_al //ข้อความที่จะส่ง
                            };

                            transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    // console.log('Email sent: ' + info.response);
                                }
                            });
                        })
                        req.flash('fail', 'ปิดงานไม่สำเร็จ!!')
                        setTimeout(() => {
                            res.redirect('/bag',);
                        }, 1000)
                    });
                });
            });
            }
        }
    } else {
        res.redirect('/');
    }
};
module.exports = controller;