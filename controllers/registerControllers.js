const controller = {};


controller.register = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM branch', (err, branch) => {
            conn.query('SELECT * FROM permission', (err, permission) => {
                if (err) console.log(err);
                else res.render('loginandregister/themplate/register', { branch: branch, permission: permission, message : req.flash('message') })
            });
        });
    });
}

controller.registersave = (req, res) => {
    const data = req.body;
    var eiei = true;
    req.getConnection((err, conn) => {
        conn.query('SELECT phone_ps FROM personnel', (err, personnel) => {
            for (var i = 0; i < personnel.length; i++) {
                if (personnel[i].phone_ps == data.phone_ps) {
                    req.flash('message', 'ข้อมูลลงทะเบียนซ้ำ')
                    res.redirect('/register');
                    eiei = false
                }
            }
            if (eiei) {
                req.getConnection((err, conn) => {
                    conn.query('INSERT INTO personnel set name_ps = ?, phone_ps = ?, branchid_ps= ?, permissionid_ps=? '
                        , [data.name_ps, data.phone_ps, data.branchid_ps, data.permissionid_ps], (err, result) => {
                            if (err) {
                                console.log(err);
                            }
                            res.render('loginandregister/validator/register_success');
                        });
                    });
                }
            })
        })
};

module.exports = controller;