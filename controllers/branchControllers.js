const controller = {};


controller.branchlist = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM branch', (err, branch) => {
                if (err) console.log(err);
                else res.render('branch/branch/branch_list', { cookie: req.cookies, branch: branch,message_edit: req.flash('message_edit'), message: req.flash('message')  })
            });
        });
    }else{
        res.redirect('/');
    }
}

controller.branchsave = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO branch SET name_br = ?'
            , [data.name_br], (err, result) => {
                if (err) {
                    console.log(err);
                }
                setTimeout(() => {
                    res.redirect('/branch',);
                }, 1000)
                req.flash('message', 'success')

            });
    });

    } else {
    res.redirect('/');
    }
};

controller.branchedit = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM branch WHERE idbranch = ?;', [id], (err, branch) => {
            if(err)console.log(err);
            else{
                res.render('branch/branch/branch_edit', {
                    branch: branch[0], cookie: req.cookies
                });
            }
            });
        });
    } else {
    res.redirect('/');
    }
};

controller.branchupdate = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('UPDATE branch SET name_br = ? WHERE idbranch = ?',
            [data.name_br, id], (err, result) => {
                if (err) {
                    res.json(err);
                }
                setTimeout(() => {
                    res.redirect('/branch',);
                }, 1000)
                req.flash('message_edit', 'editsuccess')

            });
    });
    } else {
    res.redirect('/');
    }
}

controller.branchdelete = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM branch WHERE idbranch = ?', [id], (err, result) => {
            if (err) {
                setTimeout(() => {
                    res.send(err)
                }, 500)
            }else{
                setTimeout(() => {
                    res.send(result)
                }, 1000)
            }
        });
    });
    } else {
    res.redirect('/');
    }
};

//! ---------------------------------------------------------------------------แผนก----------------------------------------------------------------

controller.departmentlist = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM department WHERE branchid_dpm= ?',[id], (err, department) => {
            conn.query('SELECT * FROM branch WHERE idbranch= ?',[id], (err, branch) => {
                if (err) console.log(err);
                else res.render('branch/department/department_list', { cookie: req.cookies, department: department,branch: branch[0], message_edit: req.flash('message_edit'), message: req.flash('message') })
            });
        });
    });
    }else{
        res.redirect('/');
    }
}

controller.departmentsave = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO department SET name_dpm = ?, branchid_dpm = ?'
            , [data.name_dpm, data.id_branch], (err, result) => {
                if (err) {
                    console.log(err);
                }
                setTimeout(() => {
                    res.redirect('/branch/department' + data.id_branch,);
                }, 1000)
                req.flash('message', 'success')

            });
    });

    } else {
    res.redirect('/');
    }
};

controller.departmentedit = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM department WHERE iddepartment = ?;', [id], (err, department) => {
            conn.query('SELECT * FROM branch;', (err1, branch) => {
            if(err || err1)console.log(err,err1);
            else{
                res.render('branch/department/department_edit', {
                    department: department[0], branch: branch, cookie: req.cookies
                });
            }
            });
        });
    });
    } else {
    res.redirect('/');
    }
};

controller.departmentupdate = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('UPDATE department SET name_dpm = ?, branchid_dpm = ? WHERE iddepartment = ?',
            [data.name_dpm,data.branchid_dpm,id], (err, result) => {
                if (err) {
                    res.json(err);
                }
                setTimeout(() => {
                    res.redirect('/branch/department'+ data.branchid_dpm,);
                }, 1000)
                req.flash('message_edit', 'editsuccess')

            });
    });
    } else {
    res.redirect('/');
    }
}

controller.departmentdelete = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM department WHERE iddepartment = ?', [id], (err, result) => {
            if (err) {
                setTimeout(() => {
                    res.send(err)
                }, 500)
            }else{
                setTimeout(() => {
                    res.send(result)
                }, 1000)
            }
        });
    });
    } else {
    res.redirect('/');
    }
};
module.exports = controller;