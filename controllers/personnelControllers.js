const controller = {};


controller.personnel_list = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch WHERE permissionid_ps = 1', (err1, personnel1) => {
                conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch WHERE permissionid_ps = 2', (err2, personnel2) => {
                    conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch WHERE permissionid_ps = 3', (err3, personnel3) => {
                        conn.query('SELECT * FROM branch', (err4, branch) => {
                            conn.query('SELECT * FROM permission', (err5, permission) => {
                                if (err1, err2, err3) console.log(err1, err2, err3)
                                else res.render('personnel/personnel_list', { cookie: req.cookies, personnel1: personnel1, personnel2: personnel2, personnel3: personnel3, branch: branch, permission: permission, message_edit: req.flash('message_edit'), message: req.flash('message') })
                            });
                        });
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.personnel_save = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO personnel SET name_ps = ? , phone_ps = ? , branchid_ps = ?, permissionid_ps = ?'
                , [data.name_ps, data.phone_ps, data.branchid_ps, data.permissionid_ps], (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    setTimeout(() => {
                        res.redirect('/personnel',);
                    }, 1000)
                    req.flash('message', 'success')

                });
        });

    } else {
        res.redirect('/');
    }
};

controller.personnel_edit = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM personnel WHERE idpersonnel = ?;', [id], (err, personnel) => {
                conn.query('SELECT * FROM branch', [id], (err, branch) => {
                    conn.query('SELECT * FROM permission', [id], (err, permission) => {
                        if (err) console.log(err);
                        else {
                            res.render('personnel/personnel_edit', {
                                personnel: personnel[0], branch: branch, permission: permission, cookie: req.cookies
                            });
                        }
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
};

controller.personnel_update = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const data = req.body;
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('UPDATE personnel SET name_ps = ? , phone_ps = ? , branchid_ps = ? , permissionid_ps = ? WHERE idpersonnel = ?',
                [data.name_ps, data.phone_ps, data.branchid_ps, data.permissionid_ps , id], (err, result) => {
                    if (err) {
                        res.json(err);
                    }
                    setTimeout(() => {
                        res.redirect('/personnel',);
                    }, 1000)
                    req.flash('message_edit', 'editsuccess')

                });
        });
    } else {
        res.redirect('/');
    }
}

controller.personnel_delete = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM personnel WHERE idpersonnel = ?', [id], (err, result) => {
                if (err) {
                    setTimeout(() => {
                        res.send(err);
                    }, 500)
                } else {
                    setTimeout(() => {
                        res.send(result);
                    }, 1000)
                }

            });
        });
    } else {
        res.redirect('/');
    }
};


module.exports = controller;