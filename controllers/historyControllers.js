const controller = {};

controller.history = (req, res) => {
    if (req.cookies.idpersonnel_ptt) {
        const idpersonnel = req.cookies.idpersonnel_ptt;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM alerts al JOIN statusalerts stal ON al.statusalertsid_al = stal.idstatusalerts JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment WHERE al.personnel_report = ? ORDER BY idalerts DESC', [idpersonnel], (err, history) => {
                conn.query('SELECT DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันรายงานเมื่อวันที่: %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา: %H:%i:%sน." )time, DATE_FORMAT(DATE_ADD(timesuccess_al, INTERVAL 543 YEAR), "ปิดงานเมื่อวันที่: %d/%m/%Y" )date_success, DATE_FORMAT(timesuccess_al, "เวลา: %H:%i:%sน." )time_success FROM alerts al JOIN statusalerts stal ON al.statusalertsid_al = stal.idstatusalerts JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment WHERE al.personnel_report = ?  ORDER BY idalerts DESC', [idpersonnel], (err, history_time) => {
                    if (err) console.log(err);
                    else {
                        res.render('alerts/history/history_list', { cookie: req.cookies, history: history,history_time: history_time })
                    }
                });
            });
        })
    } else {
        res.redirect('/');
    }
}

module.exports = controller;