const controller = {};
const request = require('request')
const uuidv4 = require('uuid').v4;
var nodemailer = require('nodemailer');


controller.notification_list = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        req.getConnection((err, conn) => {
            conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, name_ps, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment  WHERE statusalertsid_al = 1 ORDER BY idalerts DESC', (err1, alerts1) => {
                conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, ps.name_ps, ps2.name_ps name_ps2, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps2 ON ps2.idpersonnel = al.personnelid_al WHERE statusalertsid_al = 2 ORDER BY idalerts DESC', (err2, alerts2) => {
                    conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, ps.name_ps, ps2.name_ps name_ps2, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps2 ON ps2.idpersonnel = al.personnelid_al WHERE statusalertsid_al = 3 ORDER BY idalerts DESC', (err3, alerts3) => {
                        conn.query('SELECT  idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), "วันที่ %d/%m/%Y" )date, DATE_FORMAT(datetime_al, "เวลา %H:%i:%s น." )time, detail_al, ps.name_ps, ps2.name_ps name_ps2, name_br, name_dpm  FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps2 ON ps2.idpersonnel = al.personnelid_al  WHERE statusalertsid_al = 4 ORDER BY idalerts DESC', (err4, alerts4) => {
                            conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch JOIN permission pms ON ps.permissionid_ps = pms.idpermission WHERE permissionid_ps = 2 || permissionid_ps = 3', (err5, personnel) => {
                                if (err1) console.log(err1)
                                else res.render('alerts/notification/notification_list', { cookie: req.cookies, alerts1: alerts1, alerts2: alerts2, alerts3: alerts3, alerts4: alerts4, personnel: personnel, success_notification: req.flash('success_notification'), fail_notification: req.flash('fail_notification') })
                            });
                        });
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.notification_detail = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), " %d/%m/%Y" )date, DATE_FORMAT(datetime_al, " %H:%i:%s น." )time, detail_al, ps.name_ps, ps.phone_ps, ps2.idpersonnel, name_br, name_dpm, name_sta, name_group, comment_al ,ps2.name_ps name_ps2, ps2.phone_ps phone_ps2, DATE_FORMAT(DATE_ADD(timesuccess_al, INTERVAL 543 YEAR), "%d/%m/%Y" )date_success, DATE_FORMAT(timesuccess_al, "%H:%i:%s น." )time_success FROM alerts al JOIN personnel ps ON ps.idpersonnel = al.personnel_report JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN statusalerts stal ON stal.idstatusalerts = al.statusalertsid_al JOIN groupalerts gr ON gr.idgroupalerts = al.groupalertsid_al LEFT JOIN personnel ps2 ON ps2.idpersonnel = al.personnelid_al WHERE idalerts = ?', [id], (err1, detail_alerts) => {
                conn.query('SELECT * FROM alerts_img WHERE alertsid_img = ?', [id], (err2, img_alerts) => {
                    conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch JOIN permission pms ON ps.permissionid_ps = pms.idpermission WHERE permissionid_ps = 2 || permissionid_ps = 3', (err5, personnel) => {
                        conn.query('SELECT * FROM personnel ps JOIN branch br ON ps.branchid_ps = br.idbranch JOIN permission pms ON ps.permissionid_ps = pms.idpermission WHERE (permissionid_ps = 2 || permissionid_ps = 3) && idpersonnel != ?', [detail_alerts[0].idpersonnel], (err5, chang_personnel) => {
                            conn.query('SELECT * FROM alerts_img_closework WHERE alertsid_img_cw = ?', [id], (err2, img_alerts_closework) => {
                                if (err1) console.log(err1)
                                else res.render('alerts/notification/notification_detail', { cookie: req.cookies, detail_alerts: detail_alerts[0], img_alerts: img_alerts, personnel: personnel, chang_personnel: chang_personnel, img_alerts_closework: img_alerts_closework })
                            });
                        });
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.notification_delete = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM alerts_img WHERE alertsid_img = ?', [id], (err, result1) => {
                conn.query('DELETE FROM alerts WHERE idalerts = ?', [id], (err, result2) => {
                    if (err) {
                        setTimeout(() => {
                            res.send(err);
                        }, 500)
                    } else {
                        setTimeout(() => {
                            res.send(result1);
                        }, 1000)
                    }
                });
            });
        });
    } else {
        res.redirect('/');
    }
};

controller.notification_select = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { alid } = req.params;
        const { psid } = req.params;
        req.getConnection((err, conn) => {
            conn.query('UPDATE alerts SET statusalertsid_al = 2,  personnelid_al = ? WHERE idalerts = ?', [psid, alid], (err, result1) => {
                conn.query('SELECT * FROM alerts al JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment WHERE idalerts= ?', [alid], (err2, alerts) => {
                    if (err) {
                        setTimeout(() => {
                            res.send(err);
                            console.log(err);
                        }, 500)
                    } else {
                        request({
                            method: 'POST',
                            url: 'https://notify-api.line.me/api/notify',
                            header: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                            },
                            auth: {
                                bearer: alerts[0].line_token, //token
                            },
                            form: {
                                message: alerts[0].name_ps + ' ได้รับมอบหมายงาน ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm, //ข้อความที่จะส่ง
                            },
                        }, (err, httpResponse, body) => {
                            var transporter = nodemailer.createTransport({
                                host: "smtp.gmail.com",
                                port: 465,
                                secure: true,
                                auth: {
                                    user: 'pontakorn.company42@gmail.com',
                                    pass: 'pontakorn42'
                                }
                            });

                            var mailOptions = {
                                from: 'pontakorn.company42@gmail.com',
                                to: 'pontakorn322@gmail.com',
                                subject: 'มอบหมายงาน',
                                text: alerts[0].name_ps + ' ได้รับมอบหมายงาน ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm
                            };

                            transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    // console.log('Email sent: ' + info.response);
                                }
                            });
                        })
                        setTimeout(() => {
                            res.send(result1);
                        }, 1000)
                    }
                });
            });
        });
    } else {
        res.redirect('/');
    }
};

controller.notification_success = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        const data = req.body;
        if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
                var filename = uuidv4() + "." + file.name.split(".")[1];
                file.mv("./public/img_alerts_closework/" + filename, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        req.getConnection((err, conn) => {
                            conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 3, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                                conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err6, result2) => {
                                        if (err1) {
                                            console.log(err1);
                                        } else {
                                            request({
                                                method: 'POST',
                                                url: 'https://notify-api.line.me/api/notify',
                                                header: {
                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                },
                                                auth: {
                                                    bearer: alerts[0].line_token, //token
                                                },
                                                form: {
                                                    message: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al, //ข้อความที่จะส่ง
                                                },
                                            }, (err, httpResponse, body) => {
                                                var transporter = nodemailer.createTransport({
                                                    host: "smtp.gmail.com",
                                                    port: 465,
                                                    secure: true,
                                                    auth: {
                                                        user: 'pontakorn.company42@gmail.com',
                                                        pass: 'pontakorn42'
                                                    }
                                                });

                                                var mailOptions = {
                                                    from: 'pontakorn.company42@gmail.com',
                                                    to: 'pontakorn322@gmail.com',
                                                    subject: 'จัดการงานสำเร็จ!',
                                                    text: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al//ข้อความที่จะส่ง
                                                };

                                                transporter.sendMail(mailOptions, function (error, info) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        // console.log('Email sent: ' + info.response);
                                                    }
                                                });
                                            })
                                            req.flash('success_notification', 'ปิดงานสำเร็จ!!')
                                            setTimeout(() => {
                                                res.redirect('/notification',);
                                            }, 1000)
                                        }
                                    });
                                })
                            });
                        });
                    }
                });
            } else {
                //!อัพโหลดหลายรูป
                req.getConnection((err, conn) => {
                    conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 3, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                        conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                            if (err) {
                                console.log(err);
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var filename = uuidv4() + "." + file[i].name.split(".")[1];
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err, result2) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                    })
                                    file[i].mv("./public/img_alerts_closework/" + filename, async function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                    })
                                }
                                request({
                                    method: 'POST',
                                    url: 'https://notify-api.line.me/api/notify',
                                    header: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                    },
                                    auth: {
                                        bearer: alerts[0].line_token, //token
                                    },
                                    form: {
                                        message: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al, //ข้อความที่จะส่ง
                                    },
                                }, (err, httpResponse, body) => {
                                    var transporter = nodemailer.createTransport({
                                        host: "smtp.gmail.com",
                                        port: 465,
                                        secure: true,
                                        auth: {
                                            user: 'pontakorn.company42@gmail.com',
                                            pass: 'pontakorn42'
                                        }
                                    });

                                    var mailOptions = {
                                        from: 'pontakorn.company42@gmail.com',
                                        to: 'pontakorn322@gmail.com',
                                        subject: 'จัดการงานสำเร็จ!',
                                        text: alerts[0].name_ps + " ได้จัดการเหตุสำเร็จแล้ว!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดการปิดงาน: ' + data.comment_al//ข้อความที่จะส่ง
                                    };

                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            // console.log('Email sent: ' + info.response);
                                        }
                                    });
                                })
                                req.flash('success_notification', 'ปิดงานสำเร็จ!!')
                                setTimeout(() => {
                                    res.redirect('/notification',);
                                }, 1000)

                            }
                        });
                    });
                });
            }
        }
    } else {
        res.redirect('/');
    }
};

controller.notification_fail = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
        const { id } = req.params;
        const data = req.body;
        if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
                var filename = uuidv4() + "." + file.name.split(".")[1];
                file.mv("./public/img_alerts_closework/" + filename, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        req.getConnection((err, conn) => {
                            conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 4, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                                conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err6, result2) => {
                                        if (err1) {
                                            console.log(err1);
                                        } else {
                                            request({
                                                method: 'POST',
                                                url: 'https://notify-api.line.me/api/notify',
                                                header: {
                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                },
                                                auth: {
                                                    bearer: alerts[0].line_token, //token
                                                },
                                                form: {
                                                    message: alerts[0].name_ps + " ไม่สามารถจัดการงานให้สำเร็จได้!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดแก้ไขปัญหา: ' + alerts[0].comment_al, //ข้อความที่จะส่ง
                                                },
                                            }, (err, httpResponse, body) => {
                                                var transporter = nodemailer.createTransport({
                                                    host: "smtp.gmail.com",
                                                    port: 465,
                                                    secure: true,
                                                    auth: {
                                                        user: 'pontakorn.company42@gmail.com',
                                                        pass: 'pontakorn42'
                                                    }
                                                });

                                                var mailOptions = {
                                                    from: 'pontakorn.company42@gmail.com',
                                                    to: 'pontakorn322@gmail.com',
                                                    subject: 'จัดการงานไม่สำเร็จ!',
                                                    text: alerts[0].name_ps + " ไม่สามารถจัดการงานให้สำเร็จได้!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดแก้ไขปัญหา: ' + alerts[0].comment_al //ข้อความที่จะส่ง
                                                };

                                                transporter.sendMail(mailOptions, function (error, info) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        // console.log('Email sent: ' + info.response);
                                                    }
                                                });
                                            })
                                            req.flash('fail_notification', 'ปิดงานไม่สำเร็จ!!')
                                            setTimeout(() => {
                                                res.redirect('/notification',);
                                            }, 1000)

                                        }
                                    })
                                });
                            });
                        });
                    }
                });
            } else {
                //!อัพโหลดหลายรูป
                req.getConnection((err, conn) => {
                    conn.query('UPDATE alerts SET comment_al = ?, statusalertsid_al = 4, timesuccess_al = NOW() WHERE idalerts = ?', [data.comment_al, id], (err1, result1) => {
                        conn.query('SELECT * FROM alerts al JOIN branch br ON al.branchid_al = br.idbranch JOIN department dpm ON al.departmentid_al = dpm.iddepartment JOIN personnel ps ON al.personnelid_al = ps.idpersonnel JOIN groupalerts gr ON al.groupalertsid_al = gr.idgroupalerts WHERE idalerts = ?', [id], (err2, alerts) => {
                            if (err) {
                                console.log(err);
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var filename = uuidv4() + "." + file[i].name.split(".")[1];
                                    conn.query('INSERT INTO alerts_img_closework SET img_alerts_cw = ?, alertsid_img_cw = ?', [filename, id], (err, result2) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                    })
                                    file[i].mv("./public/img_alerts_closework/" + filename, async function (err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                    })
                                }
                                request({
                                    method: 'POST',
                                    url: 'https://notify-api.line.me/api/notify',
                                    header: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                    },
                                    auth: {
                                        bearer: alerts[0].line_token, //token
                                    },
                                    form: {
                                        message: alerts[0].name_ps + " ไม่สามารถจัดการงานให้สำเร็จได้!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดแก้ไขปัญหา: ' + alerts[0].comment_al, //ข้อความที่จะส่ง
                                    },
                                }, (err, httpResponse, body) => {
                                    var transporter = nodemailer.createTransport({
                                        host: "smtp.gmail.com",
                                        port: 465,
                                        secure: true,
                                        auth: {
                                            user: 'pontakorn.company42@gmail.com',
                                            pass: 'pontakorn42'
                                        }
                                    });

                                    var mailOptions = {
                                        from: 'pontakorn.company42@gmail.com',
                                        to: 'pontakorn322@gmail.com',
                                        subject: 'จัดการงานไม่สำเร็จ!',
                                        text: alerts[0].name_ps + " ไม่สามารถจัดการงานให้สำเร็จได้!! " + ' รายละเอียดเหตุ: ' + alerts[0].detail_al + ' สาขา: ' + alerts[0].name_br + ' แผนก: ' + alerts[0].name_dpm + ' รายละเอียดแก้ไขปัญหา: ' + alerts[0].comment_al //ข้อความที่จะส่ง
                                    };

                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            // console.log('Email sent: ' + info.response);
                                        }
                                    });
                                })
                                req.flash('fail_notification', 'ปิดงานไม่สำเร็จ!!')
                                setTimeout(() => {
                                    res.redirect('/notification',);
                                }, 1000)
                            }
                        });
                    });
                });
            }
        }
    } else {
        res.redirect('/');
    }
};

module.exports = controller;