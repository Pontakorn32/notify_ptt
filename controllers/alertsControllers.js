const controller = {};
const request = require('request')
const uuidv4 = require('uuid').v4;
var nodemailer = require('nodemailer');


controller.alerts = (req, res) => {
    if (req.cookies.idpersonnel_ptt) {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM branch', (err, branch) => {
                conn.query('SELECT * FROM department', (err, department) => {
                    conn.query('SELECT * FROM groupalerts WHERE enable_group = 1', (err, groupalerts) => {
                        if (err) console.log(err);
                        else res.render('alerts/form/notify', { cookie: req.cookies, branch: branch, groupalerts: groupalerts, department: department, message: req.flash('message') })
                    });
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

controller.alertssave = (req, res) => {
    if (req.cookies.idpersonnel_ptt) {
        const data = req.body;
        if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
                var filename = uuidv4() + "." + file.name.split(".")[1];
                file.mv("./public/img_alerts/" + filename, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        req.getConnection((err, conn) => {
                            conn.query('SELECT * FROM groupalerts WHERE idgroupalerts = ?', [data.groupalertsid_al], (err1, groupalerts) => {
                                conn.query('SELECT * FROM personnel WHERE idpersonnel = ?', [data.personnel_report], (err2, personnel) => {
                                    conn.query('SELECT * FROM branch WHERE idbranch = ?', [data.branchid_al], (err3, branch) => {
                                        conn.query('SELECT * FROM department WHERE iddepartment = ?', [data.departmentid_al], (err4, department) => {
                                            conn.query('INSERT INTO alerts set detail_al = ?, groupalertsid_al= ?, personnel_report=? ,branchid_al=?,departmentid_al=?, statusalertsid_al = 1'
                                                , [data.detail_al, data.groupalertsid_al, data.personnel_report, data.branchid_al, data.departmentid_al], (err5, result) => {
                                                    console.log(err5);
                                                    conn.query('INSERT INTO alerts_img SET img_alerts = ?, alertsid_img = ?', [filename, result.insertId], (err6, result2) => {
                                                        if (err1) {
                                                            console.log(err2);
                                                        } else {
                                                            request({
                                                                method: 'POST',
                                                                url: 'https://notify-api.line.me/api/notify',
                                                                header: {
                                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                                },
                                                                auth: {
                                                                    bearer: groupalerts[0].line_token, //token
                                                                },
                                                                form: {
                                                                    message: 'ผู้รายงาน: ' + personnel[0].name_ps + '  ' + 'เบอร์ติดต่อ: ' + personnel[0].phone_ps + '  ' + 'รายละเอียด: ' + data.detail_al + '  ' + 'สาขา:' + branch[0].name_br + '  ' + 'แผนก:' + '  ' + department[0].name_dpm, //ข้อความที่จะส่ง
                                                                },
                                                            }, (err, httpResponse, body) => {
                                                                var transporter = nodemailer.createTransport({
                                                                    host: "smtp.gmail.com",
                                                                    port: 465,
                                                                    secure: true,
                                                                    auth: {
                                                                        user: 'pontakorn.company42@gmail.com',
                                                                        pass: 'pontakorn42'
                                                                    }
                                                                });

                                                                var mailOptions = {
                                                                    from: 'pontakorn.company42@gmail.com',
                                                                    to: 'pontakorn322@gmail.com',
                                                                    subject: 'รายงานการแจ้งเหตุ',
                                                                    text: 'ผู้รายงาน: ' + personnel[0].name_ps + '  ' + 'เบอร์ติดต่อ: ' + personnel[0].phone_ps + '  ' + 'รายละเอียด: ' + data.detail_al + '  ' + 'สาขา:' + branch[0].name_br + '  ' + 'แผนก:' + '  ' + department[0].name_dpm
                                                                };

                                                                transporter.sendMail(mailOptions, function (error, info) {
                                                                    if (error) {
                                                                        console.log(error);
                                                                    } else {
                                                                        // console.log('Email sent: ' + info.response);
                                                                    }
                                                                });
                                                            })
                                                            setTimeout(() => {
                                                                res.redirect('/alerts',);
                                                            }, 1000)
                                                            req.flash('message', 'success')
                                                        }
                                                    });
                                                })
                                        });
                                    });
                                });
                            });
                        });
                    }
                })
            } else {
                req.getConnection((err, conn) => {
                    conn.query('SELECT * FROM groupalerts WHERE idgroupalerts = ?', [data.groupalertsid_al], (err, groupalerts) => {
                        conn.query('SELECT * FROM personnel WHERE idpersonnel = ?', [data.personnel_report], (err, personnel) => {
                            conn.query('SELECT * FROM branch WHERE idbranch = ?', [data.branchid_al], (err, branch) => {
                                conn.query('SELECT * FROM department WHERE iddepartment = ?', [data.departmentid_al], (err, department) => {
                                    conn.query('INSERT INTO alerts set detail_al = ?, groupalertsid_al= ?, personnel_report=? ,branchid_al=?,departmentid_al=?, statusalertsid_al = 1'
                                        , [data.detail_al, data.groupalertsid_al, data.personnel_report, data.branchid_al, data.departmentid_al], (err, result) => {

                                            if (err) {
                                                console.log(err);
                                            } else {
                                                for (var i = 0; i < file.length; i++) {
                                                    var filename = uuidv4() + "." + file[i].name.split(".")[1];
                                                    conn.query('INSERT INTO alerts_img SET img_alerts = ?, alertsid_img = ?', [filename, result.insertId], (err, result2) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }
                                                    })
                                                    file[i].mv("./public/img_alerts/" + filename, async function (err) {
                                                        if (err) {
                                                            console.log(err);
                                                        }

                                                    })
                                                }
                                                request({
                                                    method: 'POST',
                                                    url: 'https://notify-api.line.me/api/notify',
                                                    header: {
                                                        'Content-Type': 'application/x-www-form-urlencoded',
                                                    },
                                                    auth: {
                                                        bearer: groupalerts[0].line_token, //token
                                                    },
                                                    form: {
                                                        message: 'ผู้รายงาน: ' + personnel[0].name_ps + '  ' + 'เบอร์ติดต่อ: ' + personnel[0].phone_ps + '  ' + 'รายละเอียด: ' + data.detail_al + '  ' + 'สาขา:' + branch[0].name_br + '  ' + 'แผนก:' + '  ' + department[0].name_dpm, //ข้อความที่จะส่ง
                                                    },
                                                }, (err, httpResponse, body) => {
                                                    var transporter = nodemailer.createTransport({
                                                        host: "smtp.gmail.com",
                                                        port: 465,
                                                        secure: true,
                                                        auth: {
                                                            user: 'pontakorn.company42@gmail.com',
                                                            pass: 'pontakorn42'
                                                        }
                                                    });

                                                    var mailOptions = {
                                                        from: 'pontakorn.company42@gmail.com',
                                                        to: 'pontakorn322@gmail.com',
                                                        subject: 'รายงานการแจ้งเหตุ',
                                                        text: 'ผู้รายงาน: ' + personnel[0].name_ps + '  ' + 'เบอร์ติดต่อ: ' + personnel[0].phone_ps + '  ' + 'รายละเอียด: ' + data.detail_al + '  ' + 'สาขา:' + branch[0].name_br + '  ' + 'แผนก:' + '  ' + department[0].name_dpm
                                                    };

                                                    transporter.sendMail(mailOptions, function (error, info) {
                                                        if (error) {
                                                            console.log(error);
                                                        }
                                                        // } else {
                                                        //     console.log('Email sent: ' + info.response);
                                                        // }
                                                    });
                                                })
                                                setTimeout(() => {
                                                    res.redirect('/alerts',);
                                                }, 1000)
                                                req.flash('message', 'success')
                                            }
                                        });
                                });
                            });
                        });
                    });
                });
            }
        }

    } else {
        res.redirect('/')
    }
}


controller.department = (req, res) => {
    const { idbranch } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from department where branchid_dpm = ? ', [idbranch], (err, department) => {
            res.send(department);
        });
    });
}
module.exports = controller;