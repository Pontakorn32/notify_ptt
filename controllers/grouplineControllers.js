const controller = {};


controller.grouplinelist = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM groupalerts', (err, groupalerts) => {
                if (err) console.log(err);
                else res.render('groupalerts/groupline_list', { cookie: req.cookies, groupalerts: groupalerts,message_edit: req.flash('message_edit'), message: req.flash('message')  })
            });
        });
    }else{
        res.redirect('/');
    }
}

controller.groupalertssave = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO groupalerts SET name_group = ? , line_token = ? , enable_group = ?'
            , [data.name_group, data.line_token, data.enable_group], (err, result) => {
                if (err) {
                    console.log(err);
                }
                setTimeout(() => {
                    res.redirect('/groupline',);
                }, 1000)
                req.flash('message', 'success')

            });
    });

    } else {
    res.redirect('/');
    }
};

controller.edit = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM groupalerts WHERE idgroupalerts = ?;', [id], (err, groupalerts) => {
            if(err)console.log(err);
            else{
                res.render('groupalerts/groupline_edit', {
                    groupalerts: groupalerts[0], cookie: req.cookies
                });
            }
            });
        });
    } else {
    res.redirect('/');
    }
};

controller.update = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const data = req.body;
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('UPDATE groupalerts SET name_group = ? , line_token = ? , enable_group = ? WHERE idgroupalerts = ?',
            [data.name_group, data.line_token, data.enable_group, id], (err, result) => {
                if (err) {
                    res.json(err);
                }
                setTimeout(() => {
                    res.redirect('/groupline',);
                }, 1000)
                req.flash('message_edit', 'editsuccess')

            });
    });
    } else {
    res.redirect('/');
    }
}

controller.delete = (req, res) => {
    if (req.cookies.idpersonnel_ptt && req.cookies.permission == 3) {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM groupalerts WHERE idgroupalerts = ?', [id], (err, result) => {
            if (err) {         
                setTimeout(() => {
                    res.send(err);
                }, 500)
            } else {
                setTimeout(() => {
                    res.send(result);
                }, 1000)
            }

        });
    });
    } else {
    res.redirect('/');
    }
};


module.exports = controller;