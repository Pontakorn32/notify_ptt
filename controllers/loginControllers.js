const controller = {};

//-------------------------------------------------------------------------------Admin-------------------------------------------------------------------------------
controller.login = (req, res) => {
    res.render('loginandregister/themplate/login', { session: req.session, message: req.flash('message'), message1: req.flash('message1') });
};

controller.loginHome = (req, res) => {
    const phone_ps = req.body.phone_ps;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM personnel WHERE phone_ps= ?', [phone_ps], (err, data) => {
            if (data.length > 0) {
                if (data[0].permissionid_ps == 2 || data[0].permissionid_ps == 3) {
                    res.cookie('idpersonnel_ptt', data[0].idpersonnel, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('name_ps', data[0].name_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('permission', data[0].permissionid_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('branchid', data[0].branchid_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    conn.query('SELECT * FROM branch WHERE idbranch = ? ', [data[0].branchid_ps], (err, name_branch) => {
                        conn.query('SELECT * FROM permission WHERE idpermission = ? ', [data[0].permissionid_ps], (err, name_permission) => {
                            res.cookie('name_branch', name_branch[0].name_br, {
                                maxAge: 72 * 60 * 60 * 1000
                            })
                            res.cookie('name_permission', name_permission[0].name_pms, {
                                maxAge: 72 * 60 * 60 * 1000
                            })
                            res.redirect('/index');
                        })
                    })
                } else if (data[0].permissionid_ps == 1) {
                    res.cookie('idpersonnel_ptt', data[0].idpersonnel, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('name_ps', data[0].name_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('permission', data[0].permissionid_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    res.cookie('branchid', data[0].branchid_ps, {
                        maxAge: 72 * 60 * 60 * 1000
                    });
                    conn.query('SELECT * FROM branch WHERE idbranch = ? ', [data[0].branchid_ps], (err, name_branch) => {
                        conn.query('SELECT * FROM permission WHERE idpermission = ? ', [data[0].permissionid_ps], (err, name_permission) => {
                            res.cookie('name_branch', name_branch[0].name_br, {
                                maxAge: 72 * 60 * 60 * 1000
                            })
                            res.cookie('name_permission', name_permission[0].name_pms, {
                                maxAge: 72 * 60 * 60 * 1000
                            })
                            res.redirect('/alerts');
                        })
                    })
                }
            } else {
                req.flash('message', 'ข้อมูลเบอร์โทรศัพท์ไม่ถูกต้อง!!')
                res.redirect('/');
            }
        });
    });
};

controller.index = (req, res) => {
    if (req.cookies.idpersonnel_ptt) {
        req.getConnection((err, conn) => {
            conn.query('SELECT idalerts, DATE_FORMAT(DATE_ADD(datetime_al, INTERVAL 543 YEAR), " %d/%m/%Y" )date, DATE_FORMAT(datetime_al, " %H:%i:%s น." )time, detail_al, ps.name_ps, ps.phone_ps, name_br, name_dpm, name_sta, statusalertsid_al FROM alerts al JOIN statusalerts sta ON sta.idstatusalerts = al.statusalertsid_al JOIN branch br ON br.idbranch = al.branchid_al JOIN department dpm ON dpm.iddepartment = al.departmentid_al JOIN personnel ps ON ps.idpersonnel = al.personnel_report WHERE statusalertsid_al = 1 OR statusalertsid_al = 2 ORDER BY idalerts desc ', (err, alerts) => {
                conn.query('SELECT * FROM alerts al JOIN personnel ps ON al.personnelid_al = ps.idpersonnel WHERE statusalertsid_al = 2 ORDER BY idalerts ASC ', (err, assignment) => {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render('index/index', { alerts: alerts, assignment:assignment, cookie: req.cookies });
                    }
                });
            });
        });
    }

};

controller.logout = function (req, res) {
    req.session.destroy();
    res.clearCookie('idpersonnel_ptt');
    res.clearCookie('name_ps');
    res.clearCookie('permission');
    res.clearCookie('branchid');
    res.clearCookie('name_branch');
    res.clearCookie('name_permission');
    res.redirect('/');
};

module.exports = controller;