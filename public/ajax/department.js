$('#branch').change(function () {
    getdepartment();
})
function getdepartment() {
    const idbranch = $('#branch').val();
    $.ajax({
        type: 'GET',
        url: '/api/department/' + idbranch,
        success: function (result) {
            $('#department').empty();
            $('#department').append('<option value="">กรุณาเลือกแผนก</option>');
            $.each(result, function (i, department) {
                $('#department').append('<option value=' + department.iddepartment + '>' + department.name_dpm + '</option>');
            });
            // console.log(result);
        },
        error: function (err) {
            console.log(err);
        }
    })
}
