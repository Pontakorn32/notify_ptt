$(document).ready(function() {
    $('#datatable').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-assignment').DataTable({
        "searching": false,
        "ordering": false,
        "info":     false,
        "lengthChange": false
    });
  } );

  //!--------------------------------------------------------------data table personnel-----------------------------------------------------

  $(document).ready(function() {
    $('#datatable-personnel').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-manager').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-admin').DataTable();
  } );

  //!--------------------------------------------------------------data table notification-----------------------------------------------------

  $(document).ready(function() {
    $('#datatable-wait').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-do').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-success').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-fail').DataTable();
  } );

  //!--------------------------------------------------------------data table bag-----------------------------------------------------

  $(document).ready(function() {
    $('#datatable-bag-wait').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-bag-success').DataTable();
  } );

  $(document).ready(function() {
    $('#datatable-bag-fail').DataTable();
  } );

//!--------------------------------------------------------------data table history-----------------------------------------------------


  $(document).ready(function() {
    $('#datatable-history').DataTable({
        "info":     false,
        "lengthChange": false
    });
  } );

  //!--------------------------------------------------------------data table notification assignment-----------------------------------------------------


  $(document).ready(function() {
    $('#datatable-assignment-personnel').DataTable({
      "info":     false,
      "ordering": false,
      "lengthChange": false
    });
    
  } );

  //!--------------------------------------------------------------data table notification detail assignment-----------------------------------------------------


  $(document).ready(function() {
    $('#datatable-assignment-personnel-detail').DataTable({
      "info":     false,
      "ordering": false,
      "lengthChange": false
    });
    
  } );

  $(document).ready(function() {
    $('#datatable-assignment-personnel-detail-chang').DataTable({
      "info":     false,
      "ordering": false,
      "lengthChange": false
    });
    
  } );