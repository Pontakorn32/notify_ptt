//!-----------------------------------------------------------ลบสาขา-----------------------------------------------------
$(document).ready(function () {
    $('.delete_branch').click(function () {
        var uid = $(this).attr("id");
        swal.fire({
            title: "ลบรายการนี้หรือไม่?",
            text: "เมื่อรายการนี้ถูกลบ คุณไม่สามารถกู้คืนได้!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "ตกลง",
            cancelButtonColor: '#d33',
            cancelButtonText: "ยกเลิก",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/branch/delete" + uid,
                    success: function (result) {
                        if (result.errno == 1451) {
                            swal.fire({
                                icon: "error",
                                title: "ไม่สามารถลบข้อมูลนี้ได้",
                                showConfirmButton: false,
                                timer: 2500
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                text: 'ท่านได้ทำการลบสำเร็จแล้ว',
                                timer: 2500
                            });
                            setTimeout(
                                function () {
                                    location.href = "/branch"
                                }, 2500)
                        }

                    },
                })
            }else{
                swal.fire({
                    icon: "warning",
                    title: "ยกเลิกการลบ",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });

    });
})

//!-----------------------------------------------------------ลบไลน์กรุ๊ป-----------------------------------------------------

$(document).ready(function () {
    $('.delete_groupline').click(function () {
        var uid = $(this).attr("id");
        swal.fire({
            title: "ลบรายการนี้หรือไม่?",
            text: "เมื่อรายการนี้ถูกลบ คุณไม่สามารถกู้คืนได้!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "ตกลง",
            cancelButtonColor: '#d33',
            cancelButtonText: "ยกเลิก",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/groupline/delete" + uid,
                    success: function (result) {
                        if (result.errno == 1451) {
                            swal.fire({
                                icon: "error",
                                title: "ไม่สามารถลบข้อมูลนี้ได้",
                                showConfirmButton: false,
                                timer: 2500
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                text: 'ท่านได้ทำการลบสำเร็จแล้ว',
                                timer: 2500
                            });
                            setTimeout(
                                function () {
                                    location.href = "/groupline"
                                }, 2500)
                        }
                    },
                })
            }else{
                swal.fire({
                    icon: "warning",
                    title: "ยกเลิกการลบ",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });

    });
})

//!-----------------------------------------------------------ลบแผนก-----------------------------------------------------

$(document).ready(function () {
    $('.delete_department').click(function () {
        var uid = $(this).attr("id");
        var backid = $('.back_id').attr("value");
        swal.fire({
            title: "ลบรายการนี้หรือไม่?",
            text: "เมื่อรายการนี้ถูกลบ คุณไม่สามารถกู้คืนได้!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "ตกลง",
            cancelButtonColor: '#d33',
            cancelButtonText: "ยกเลิก",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/branch/department/delete" + uid,
                    success: function (result) {
                        if (result.errno == 1451) {
                            swal.fire({
                                icon: "error",
                                title: "ไม่สามารถลบข้อมูลนี้ได้",
                                showConfirmButton: false,
                                timer: 2500
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                text: 'ท่านได้ทำการลบสำเร็จแล้ว',
                                timer: 2500
                            });
                            setTimeout(
                                function () {
                                    location.href = "/branch/department" + backid
                                }, 2500)
                        }
                    },
                })
            }else{
                swal.fire({
                    icon: "warning",
                    title: "ยกเลิกการลบ",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });

    });
})

//!-----------------------------------------------------------ลบสมาชิก-----------------------------------------------------

$(document).ready(function () {
    $('.delete_personnel').click(function () {
        var uid = $(this).attr("id");
        swal.fire({
            title: "ลบรายการนี้หรือไม่?",
            text: "เมื่อรายการนี้ถูกลบ คุณไม่สามารถกู้คืนได้!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "ตกลง",
            cancelButtonColor: '#d33',
            cancelButtonText: "ยกเลิก",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/personnel/delete" + uid,
                    success: function (result) {
                        if (result.errno == 1451) {
                            swal.fire({
                                icon: "error",
                                title: "ไม่สามารถลบข้อมูลนี้ได้",
                                showConfirmButton: false,
                                timer: 2500
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                text: 'ท่านได้ทำการลบสำเร็จแล้ว',
                                timer: 2500
                            });
                            setTimeout(
                                function () {
                                    location.href = "/personnel"
                                }, 2500)
                        }
                    },
                })
            }else{
                swal.fire({
                    icon: "warning",
                    title: "ยกเลิกการลบ",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });

    });
})

//!-----------------------------------------------------------ลบข้อมูลการแจ้งเหตุ-----------------------------------------------------

$(document).ready(function () {
    $('.delete_notification').click(function () {
        var uid = $(this).attr("id");
        swal.fire({
            title: "ลบรายการนี้หรือไม่?",
            text: "เมื่อรายการนี้ถูกลบ คุณไม่สามารถกู้คืนได้!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "ตกลง",
            cancelButtonColor: '#d33',
            cancelButtonText: "ยกเลิก",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "GET",
                    url: "/notification/delete" + uid,
                    success: function (result) {
                        if (result.errno == 1451) {
                            swal.fire({
                                icon: "error",
                                title: "ไม่สามารถลบข้อมูลนี้ได้",
                                showConfirmButton: false,
                                timer: 2500
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: 'Deleted!',
                                text: 'ท่านได้ทำการลบสำเร็จแล้ว',
                                timer: 2500
                            });
                            setTimeout(
                                function () {
                                    location.href = "/notification"
                                }, 2500)
                        }
                    },
                })
            }else{
                swal.fire({
                    icon: "warning",
                    title: "ยกเลิกการลบ",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });

    });
})