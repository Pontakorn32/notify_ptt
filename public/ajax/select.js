//!-----------------------------------------------------------เลือกเจ้าหน้าที่รับผิดชอบงาน-----------------------------------------------------
$(document).ready(function () {
    $('.assignment').click(function () {
        var alid = $(this).attr("id");
        $('.select_personnel').click(function () {
            var psid = $(this).attr("id");
            var name = $(this).attr("name");
            swal.fire({
                title: "ทำการเลือกผู้รับผิดชอบงาน!",
                text: "คุณต้องการให้ " + name + " เป็นผู้รับผิดชอบงานใช่หรือไม่?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: "ตกลง",
                cancelButtonColor: '#d33',
                cancelButtonText: "ยกเลิก",
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "GET",
                        url: "/notification/select" + alid + "/" + psid,
                        success: function (result) {
                            if (result.errno) {
                                swal.fire({
                                    icon: "error",
                                    title: "ทำการเลือกล้มเหลว",
                                    showConfirmButton: false,
                                    timer: 2500
                                });
                            } else {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success!',
                                    text: 'ท่านได้ทำการมอบหมายงานสำเร็จแล้ว',
                                    timer: 2500
                                });
                                setTimeout(
                                    function () {
                                        location.href = "/notification"
                                    }, 2500)
                            }

                        },
                    })
                } else {
                    swal.fire({
                        icon: "warning",
                        title: "คุณได้ทำการยกเลิก",
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            });

        });
    })
})

//!--------------------------------------------------------------------ปิดงานสำเร็จ-----------------------------------------------------
// $(document).ready(function () {
//     $('.success').click(function () {
//         var alid = $(this).attr("id");
//         $('#form-success').submit(function () {
//             $.ajax({
//                 type: "POST",
//                 url: '/notification/success' + alid,
//                 data: $(this).serialize(), // serializes the form's elements.
//                 success: function (alerts) {
//                     swal.fire({
//                         icon: "success",
//                         title: "ทำการปิดงานสำเร็จเรียบร้อย",
//                         showConfirmButton: true,
//                         timer: 2500
//                     });
//                     setTimeout(
//                         function () {
//                             location.href = "/notification"
//                         }, 2500)
//                 }
//             })
//             event.preventDefault();
//         })
//     })
// })

//!--------------------------------------------------------------------ปิดงานไม่สำเร็จ-----------------------------------------------------
// $(document).ready(function () {
//     $('.fail').click(function () {
//         var alid = $(this).attr("id");
//         $('#form-fail').submit(function () {
//             $.ajax({
//                 type: "POST",
//                 url: '/notification/fail' + alid,
//                 data: $(this).serialize(), // serializes the form's elements.
//                 success: function (alerts) {
//                     swal.fire({
//                         icon: "warning",
//                         title: "ทำการปิดงานไม่สำเร็จ!!",
//                         showConfirmButton: true,
//                         timer: 2500
//                     });
//                     setTimeout(
//                         function () {
//                             location.href = "/notification"
//                         }, 2500)
//                 }
//             })
//             event.preventDefault();
//         })
//     })
// })

//!--------------------------------------------------------------------กระเป๋าปิดงานสำเร็จ-----------------------------------------------------
// $(document).ready(function () {
//         $('.success-bag').click(function () {
//             var alid = $(this).attr("id");
//             $('#form-success-bag').submit(function () {
//                 formdata.append('name', 'value');
//                 $.ajax({
//                 type: "POST",
//                 url: '/bag/success' + alid,
//                 data: formdata, // serializes the form's elements.
//                 success: function (alerts) {
//                     swal.fire({
//                         icon: "warning",
//                         title: "ทำการปิดงานสำเร็จ!!",
//                         showConfirmButton: true,
//                         timer: 2500
//                     });
//                     setTimeout(
//                         function () {
//                             location.href = "/bag"
//                         }, 2500)
//                 }
//             })
//             event.preventDefault();
//             })
//         })
// })

//!--------------------------------------------------------------------กระเป๋าปิดงานไม่สำเร็จ-----------------------------------------------------
// $(document).ready(function () {
//     $('.fail-bag').click(function () {
//         var alid = $(this).attr("id");
//         $('#form-fail-bag').submit(function () {
//             $.ajax({
//                 type: "POST",
//                 url: '/bag/fail' + alid,
//                 data: $(this).serialize(), // serializes the form's elements.
//                 success: function (alerts) {
//                     swal.fire({
//                         icon: "warning",
//                         title: "ทำการปิดงานไม่สำเร็จ!!",
//                         showConfirmButton: true,
//                         timer: 2500
//                     });
//                     setTimeout(
//                         function () {
//                             location.href = "/bag"
//                         }, 2500)
//                 }
//             })
//             event.preventDefault();
//         })
//     })
// })